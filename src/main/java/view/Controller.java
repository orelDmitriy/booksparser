package view;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.OutFieldStructure;
import model.RegExpsStructure;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.RootService;
import util.Copy;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class Controller {

    private static final Logger logger = LoggerFactory.getLogger(Controller.class);
    private static final String ROOT_DIRECTORY = "D:\\парсер\\первый";
    @FXML
    public TreeTableView<RegExpsStructure> regExpsStructureTreeTableView;

    @FXML
    public TreeTableColumn<RegExpsStructure, String> regExpsStructureName;

    @FXML
    public TreeTableColumn<RegExpsStructure, String> regExpsStructureBeginPatternColumn;

    @FXML
    public TreeTableColumn<RegExpsStructure, String> regExpsStructureEndPatternColumn;

    @FXML
    public TextField regExpsStructureNewName;

    @FXML
    public TextField regExpsStructureBeginPattern;

    @FXML
    public TextField regExpsStructureEndPattern;

    @FXML
    public TableView<OutFieldStructure> outFieldStructureTableView;

    @FXML
    public TableColumn<OutFieldStructure, String> outFieldStructurePosition;

    @FXML
    public TableColumn<OutFieldStructure, String> outFieldStructureName;

    @FXML
    public TableColumn<OutFieldStructure, String> outFieldStructurePattern;

    @FXML
    public TextField outFieldStructureNewName;

    @FXML
    public TextField outFieldStructureNewPattern;

    @FXML
    public TextField outFieldStructureNewNumber;

    @FXML
    public TextField inputFile;

    @FXML
    public TextField outFile;

    @FXML
    public ComboBox<String> buffer;

    @FXML
    public RadioButton fullText;

    @FXML
    public RadioButton clearText;

    private RootService rootService;

    @FXML
    public void initialize() {
        regExpsStructureName.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getName())
        );
        regExpsStructureBeginPatternColumn.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getRegExpStart())
        );
        regExpsStructureEndPatternColumn.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getValue().getRegExpEnd())
        );

        outFieldStructurePosition.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getNumber().toString())
        );
        outFieldStructureName.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getFieldName())
        );
        outFieldStructurePattern.setCellValueFactory(
                param ->
                        new ReadOnlyStringWrapper(param.getValue().getPattern())
        );

        regExpsStructureTreeTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (observable.getValue() == null) {
                return;
            }
            RegExpsStructure value = observable.getValue().getValue();
            regExpsStructureNewName.setText(value.getName());
            regExpsStructureBeginPattern.setText(value.getRegExpStart());
            regExpsStructureEndPattern.setText(value.getRegExpEnd());

        });

        outFieldStructureTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (observable.getValue() == null) {
                return;
            }
            OutFieldStructure value = observable.getValue();
            outFieldStructureNewName.setText(value.getFieldName());
            outFieldStructureNewNumber.setText(value.getNumber().toString());
            outFieldStructureNewPattern.setText(value.getPattern());
        });

        rootService = new RootService();
        updateRegExpsStructure();
        ObservableList<String> patternName = FXCollections.observableArrayList();
        buffer.setItems(patternName);
        buffer.getSelectionModel().selectedIndexProperty().addListener((obs, oldSelection, newSelection) -> setBuffer((Integer) newSelection));
        setRadioButton();
        refresh();
        setDataForBuffer();
    }

    public void rewriteRegExpsStructure() {
        if (isNotEmpty(regExpsStructureNewName) && isNotEmpty(regExpsStructureBeginPattern)) {
            TreeItem<RegExpsStructure> root = regExpsStructureTreeTableView.getRoot();
            String regExpsStructureName = regExpsStructureNewName.getText();
            RegExpsStructure regExpsStructure = regExpsStructureTreeTableView.getFocusModel().getFocusedItem().getValue();
            if (regExpsStructureName.equals(regExpsStructure.getName()) || isFreeNameRegExpsStructure(root, regExpsStructureName)) {
                rootService.rewrite(regExpsStructure, regExpsStructureName, regExpsStructureBeginPattern.getText(), regExpsStructureEndPattern.getText());
                refresh();
                setDataForBuffer();
            } else {
                getErrorDialog("Имя занято.");
            }
        } else {
            getErrorDialog("Заполните поля.");
        }
    }

    public void deleteRegExpsStructure() {
        TreeItem<RegExpsStructure> root = regExpsStructureTreeTableView.getRoot();
        if (root.equals(regExpsStructureTreeTableView.getFocusModel().getFocusedItem())) {
            getErrorDialog("Корень удалять нельзя.");
            return;
        }
        TreeItem<RegExpsStructure> focusedItem = regExpsStructureTreeTableView.getFocusModel().getFocusedItem();
        rootService.deleteRegExpsStructure(focusedItem.getValue());
        focusedItem.getParent().getChildren().remove(regExpsStructureTreeTableView.getFocusModel().getFocusedItem());
        refresh();
        setDataForBuffer();
    }

    public void addChildForRegExpsStructure() {
        if (isNotEmpty(regExpsStructureNewName) && isNotEmpty(regExpsStructureBeginPattern)) {
            TreeItem<RegExpsStructure> root = regExpsStructureTreeTableView.getRoot();
            String regExpsStructureName = regExpsStructureNewName.getText();
            RegExpsStructure regExpsStructure = regExpsStructureTreeTableView.getFocusModel().getFocusedItem().getValue();
            if (isFreeNameRegExpsStructure(root, regExpsStructureName)) {
                RegExpsStructure newRegExpsStructureNode = new RegExpsStructure(regExpsStructureName, regExpsStructureBeginPattern.getText(), regExpsStructureEndPattern.getText());
                regExpsStructureTreeTableView.getFocusModel().getFocusedItem().getChildren().add(new TreeItem<>(newRegExpsStructureNode));
                rootService.addChild(regExpsStructure, newRegExpsStructureNode);
                refresh();
                setDataForBuffer();
            } else {
                getErrorDialog("Имя занято.");
            }
        } else {
            getErrorDialog("Заполните поля.");
        }
    }

    public void rewriteOutFieldStructure() {
        if (isNotEmpty(outFieldStructureNewName) && isNotEmpty(outFieldStructureNewPattern) && isNotEmpty(outFieldStructureNewNumber)) {
            Integer fieldNumber;
            OutFieldStructure current = outFieldStructureTableView.getFocusModel().getFocusedItem();
            try {
                fieldNumber = Integer.parseInt(outFieldStructureNewNumber.getText());
            } catch (NumberFormatException e) {
                getErrorDialog("В поле \"Номер:\" должно быть целое число.");
                return;
            }
            if (!current.getFieldName().equals(outFieldStructureNewName.getText()) && !isFreeNameOutFieldStructure(outFieldStructureNewName.getText())) {
                getErrorDialog("Имя занято.");
                return;
            }
            if (!current.getNumber().equals(fieldNumber) && !isFreeNumberOutFieldStructure(fieldNumber)) {
                getErrorDialog("Номер занят.");
                return;
            }
            rootService.updateOutFieldStructure(current, outFieldStructureNewName.getText(), outFieldStructureNewPattern.getText(), fieldNumber);
            refresh();
        } else {
            getErrorDialog("Заполните поля.");
        }
    }

    public void deleteOutFieldStructure() {
        OutFieldStructure current = outFieldStructureTableView.getFocusModel().getFocusedItem();
        outFieldStructureTableView.getItems().remove(current);
        rootService.deleteOutFieldStructure(current);
        refresh();
    }

    public void addChildForOutFieldStructure() {
        if (isNotEmpty(outFieldStructureNewName) && isNotEmpty(outFieldStructureNewPattern) && isNotEmpty(outFieldStructureNewNumber)) {
            Integer fieldNumber;
            try {
                fieldNumber = Integer.parseInt(outFieldStructureNewNumber.getText());
            } catch (NumberFormatException e) {
                getErrorDialog("В поле \"Номер:\" должно быть целое число.");
                return;
            }
            if (!isFreeNameOutFieldStructure(outFieldStructureNewName.getText())) {
                getErrorDialog("Имя занято.");
                return;
            }
            fieldNumber = getUniqueNumber(fieldNumber);
            rootService.addOutFieldStructure(outFieldStructureNewName.getText(), outFieldStructureNewPattern.getText(), fieldNumber);
            refresh();
        } else {
            getErrorDialog("Заполните поля.");
        }
    }

    public void chooseInputFile() {
        String filePath = getFilePath();
        inputFile.setText(filePath);
    }

    public void chooseOutFile() {
        String filePath = getFilePath();
        outFile.setText(filePath);
    }

    public void process() {
        if (isEmpty(inputFile)) {
            getErrorDialog("Выберите исходный файл.");
            return;
        }
        if (isEmpty(outFile)) {
            getErrorDialog("Выберите куда сохранить.");
            return;
        }
        if (outFieldStructureTableView.getItems().size() == 0) {
            getErrorDialog("Выходные данные пусты.");
            return;
        }
        try {
            rootService.process(inputFile.getText(), outFile.getText());
        } catch (IOException e) {
            logger.error("Не удалось записать файл.", e);
            getErrorDialog("Не удалось записать файл.");
            return;
        }
        getInfoDialog("Сохранено");
    }

    public void replaceAbbreviations(){
        if (isEmpty(inputFile)) {
            getErrorDialog("Выберите исходный файл.");
            return;
        }
        if (isEmpty(outFile)) {
            getErrorDialog("Выберите куда сохранить.");
            return;
        }
        try {
            rootService.replaceAbbreviations(inputFile.getText(), outFile.getText());
        } catch (Exception e) {
            logger.error("Не удалось заменить аббревиатуры.", e);
            getErrorDialog("Не удалось заменить аббревиатуры.");
        }
        getInfoDialog("Сохранено");
    }

    public void saveSettings() {
        String path = getFilePath();
        if ("".equals(path)) {
            getErrorDialog("Файл для сохранения не выбран.");
            return;
        }
        try {
            rootService.saveSettings(path);
        } catch (Exception e) {
            logger.error("Ошбика сохранения в файл.", e);
            getErrorDialog("Ошбика сохранения в файл.");
            return;
        }
        getInfoDialog("Сохранено");
    }

    public void loadSettings() {
        String path = getFilePath();
        if ("".equals(path)) {
            getErrorDialog("Файл для загрузки не выбран.");
            return;
        }
        try {
            rootService.loadSettings(path);
        } catch (Exception e) {
            logger.error("Ошбика загрузки настроек.", e);
            getErrorDialog("Ошбика загрузки настроек.");
            return;
        }
        updateRegExpsStructure();
        refresh();
        setDataForBuffer();
        getInfoDialog("Настройки загруженны.");
    }

    public void loadAbbreviations() {
        String path = getFilePath();
        if ("".equals(path)) {
            getErrorDialog("Файл для загрузки не выбран.");
            return;
        }
        try {
            rootService.loadAbbreviations(path);
        } catch (Exception e) {
            getErrorDialog("Ошибка при загрузке файла.");
            logger.error("Ошибка при загрузке файла.", e);
            return;
        }
        getInfoDialog("Файл загружен.");
    }

    private void setRadioButton() {
        final ToggleGroup group = new ToggleGroup();
        fullText.setToggleGroup(group);
        fullText.setSelected(true);
        clearText.setToggleGroup(group);
        group.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            newValue.setSelected(true);
            setBuffer(null);
        });
    }

    private void setBuffer(Integer newSelection) {
        if (newSelection == null) {
            newSelection = buffer.getSelectionModel().getSelectedIndex();
        }
        if (newSelection > -1) {
            if (fullText.isSelected()) {
                Copy.setClipboard(OutFieldStructure.VARIABLE_NAME_BORDER + buffer.getItems().get(newSelection) + OutFieldStructure.VARIABLE_NAME_BORDER);
            } else {
                Copy.setClipboard(OutFieldStructure.VARIABLE_VALUE_BORDER + buffer.getItems().get(newSelection) + OutFieldStructure.VARIABLE_VALUE_BORDER);
            }
        }
    }

    private void refresh() {
        setRegExpsStructure();
        setOutFieldStructure();
    }

    private void setRegExpsStructure() {
        regExpsStructureTreeTableView.refresh();
    }

    private TreeItem<RegExpsStructure> getItemsWithChildren(RegExpsStructure rootRegExpsStructure) {
        TreeItem<RegExpsStructure> currentItem = new TreeItem<>(rootRegExpsStructure);
        for (RegExpsStructure child : rootRegExpsStructure.getChildren()) {
            TreeItem<RegExpsStructure> childItem = getItemsWithChildren(child);
            currentItem.getChildren().add(childItem);
        }
        return currentItem;
    }

    private void setOutFieldStructure() {
        ObservableList<OutFieldStructure> outFieldStructures = FXCollections.observableArrayList();
        outFieldStructureTableView.setItems(outFieldStructures);
        outFieldStructures.addAll(rootService.getOutFieldStructures());
        outFieldStructureTableView.refresh();
    }

    private String getFilePath() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setCurrentDirectory(new File(ROOT_DIRECTORY));
        fileChooser.showOpenDialog(null);
        String path = "";
        if (fileChooser.getSelectedFile() != null) {
            path = fileChooser.getSelectedFile().getAbsolutePath();
        }
        return path;
    }

    private void getInfoDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Информация");
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    private void getErrorDialog(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(message);
        alert.showAndWait();
    }

    private boolean isNotEmpty(TextField field) {
        return !"".equals(field.getText());
    }

    private boolean isEmpty(TextField field) {
        return "".equals(field.getText());
    }

    private boolean isFreeNameRegExpsStructure(TreeItem<RegExpsStructure> root, String name) {
        if (root.getValue().getName().equals(name)) {
            return false;
        }
        boolean result;
        for (TreeItem<RegExpsStructure> child : root.getChildren()) {
            result = isFreeNameRegExpsStructure(child, name);
            if (!result) {
                return false;
            }
        }
        return true;
    }

    private boolean isFreeNameOutFieldStructure(String outFieldStructureNewNameText) {
        for (OutFieldStructure outFieldStructure : outFieldStructureTableView.getItems()) {
            if (outFieldStructureNewNameText.equals(outFieldStructure.getFieldName())) {
                return false;
            }
        }
        return true;
    }

    private boolean isFreeNumberOutFieldStructure(Integer newNumber) {
        for (OutFieldStructure outFieldStructure : outFieldStructureTableView.getItems()) {
            if (Objects.equals(newNumber, outFieldStructure.getNumber())) {
                return false;
            }
        }
        return true;
    }

    private void setDataForBuffer() {
        buffer.getItems().clear();
        RegExpsStructure root = regExpsStructureTreeTableView.getRoot().getValue();
        addItems(root);
    }

    private void addItems(RegExpsStructure root) {
        buffer.getItems().add(root.getName());
        for (RegExpsStructure child : root.getChildren()) {
            addItems(child);
        }
    }

    private void updateRegExpsStructure() {
        RegExpsStructure rootRegExpsStructure = rootService.getRootRegExpsStructure();
        TreeItem<RegExpsStructure> root = getItemsWithChildren(rootRegExpsStructure);
        regExpsStructureTreeTableView.setRoot(root);
    }

    private Integer getUniqueNumber(Integer fieldNumber) {
        if (!isFreeNumberOutFieldStructure(fieldNumber)) {
            return getUniqueNumber(++fieldNumber);
        }
        return fieldNumber;
    }
}
