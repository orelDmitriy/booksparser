package model;

import java.io.Serializable;

public class OutFieldStructure implements Serializable {

    public static final String VARIABLE_NAME_BORDER = "/#";
    public static final String VARIABLE_VALUE_BORDER = "/c#";

    private String fieldName;
    private String pattern;
    private int number;

    public OutFieldStructure(String fieldName, String pattern, int number) {
        this.fieldName = fieldName;
        this.pattern = pattern;
        this.number = number;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getPattern() {
        return pattern;
    }

    public Integer getNumber() {
        return number;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
