package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp {

    public static int[] findFirst(String pattern, String line) {
        Pattern pat = Pattern.compile(pattern);
        Matcher matcher = pat.matcher(line);
        if (matcher.find()){
            return new int[]{matcher.start(), matcher.end()};
        } else {
            return new int[]{0,-1};
        }
    }
}
