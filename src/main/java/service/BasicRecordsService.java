package service;

import model.BasicRecord;
import model.RegExpsStructure;
import util.RegExp;

import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BasicRecordsService {

    public final static String ENCODING = "UTF-8";//"UTF-8"; //"Cp1252";

    private static String currentLine;

    public static List<BasicRecord> getAllRecords(File in, RegExpsStructure regExpsStructure) throws IOException {

        List<BasicRecord> records = getArticle(in, regExpsStructure);

        for (BasicRecord record : records) {
            Map<String, String[]> fields = addAllFields(record.getFields().get(regExpsStructure.getName())[0] + record.getFields().get(regExpsStructure.getName())[1], regExpsStructure);
            record.getFields().putAll(fields);
        }
        return records;
    }

    public static Map<String, String[]> addAllFields(String text, RegExpsStructure regExpsStructure) {
        Map<String, String[]> fields = new HashMap<>();
        for (RegExpsStructure child : regExpsStructure.getChildren()) {
            int[] first = RegExp.findFirst(child.getRegExpStart(), text);
            if (first[1] != -1) {
                String key = text.substring(first[0], first[1]);
                String tail = text.substring(first[1]);
                int endIndex = first[0] + getEndIndex(child, tail);
                String textForChild = text.substring(first[1], endIndex + first[1] - first[0]).trim();
                text = cutText(text, first[0], endIndex + first[1] - first[0]);
                fields.put(child.getName(), new String[]{key, textForChild});
                fields.putAll(addAllFields(key + textForChild, child));
            } else {
                fields.put(child.getName(), new String[]{"", ""});
                fields.putAll(addAllFields("", child));
            }
        }
        return fields;
    }

    private static String cutText(String text, int starsIndex, int endIndex) {
        return text.substring(0, starsIndex) + text.substring(endIndex);
    }

    private static int getEndIndex(RegExpsStructure child, String tail) {
        int minimum = Integer.MAX_VALUE;
        for (String pattern : child.getAllRegExpsForEnd()) {
            int[] position = RegExp.findFirst(pattern, tail);
            if (position[1] != -1 && position[0] < minimum) {
                minimum = position[0];
            }
        }
        if (minimum == Integer.MAX_VALUE) {
            minimum = tail.length();
        }
        return minimum;
    }

    private static List<BasicRecord> getArticle(File in, RegExpsStructure regExpsStructure) throws IOException {
        List<BasicRecord> article = new LinkedList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(in), ENCODING));
        currentLine = br.readLine();
        while (currentLine != null) {
            int[] first = RegExp.findFirst(regExpsStructure.getRegExpStart(), currentLine);
            if ( first[1] != -1) {
                BasicRecord record = new BasicRecord();
                String tail = currentLine.substring(first[1]);
                String articleText;
                String key = currentLine.substring(first[0], first[1]);
                if ("".equals(regExpsStructure.getRegExpEnd())) {
                    articleText = getBlock(br, tail, regExpsStructure.getRegExpStart(), 0);
                } else {
                    articleText = getBlock(br, tail, regExpsStructure.getRegExpEnd(), 0);
                }
                record.addField(regExpsStructure.getName(), new String[]{key, articleText});
                article.add(record);
            } else {
                currentLine = br.readLine();
            }
        }
        br.close();
        return article;
    }

    private static String getBlock(BufferedReader br, String line, String pattern, int depth) throws IOException {
        int[] first = RegExp.findFirst(pattern, line);
        if (first[1] == -1) {
            String nextLine = br.readLine();
            currentLine = nextLine;
            if (nextLine != null) {
                return line + "\n" + getBlock(br, nextLine, pattern, ++depth);
            }
            return line;
        } else {
            if (line.isEmpty()){
                currentLine = br.readLine();
            } else {
                currentLine = line.substring(first[0]);
            }
            return line.substring(0, first[0]);
        }
    }
}
