import service.BasicRecordsService;
import service.OutService;
import model.BasicRecord;
import model.Out;
import model.OutFieldStructure;
import model.RegExpsStructure;
import org.junit.Test;
import util.RegExp;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class BasicRecordsServiceTest {

    @Test
    public void emptyKeyOrValue() throws Exception {
        RegExpsStructure root = new RegExpsStructure("root", "", "");
        RegExpsStructure regExpsStructure = new RegExpsStructure("test", "^", "");
        root.addChild(regExpsStructure);
        Map<String, String[]> stringMap = BasicRecordsService.addAllFields("123", root);

        stringMap.forEach((s, strings) -> {
            System.out.println("name: " + s);
            System.out.println("key: " + strings[0]);
            System.out.println("value: " + strings[1]);
            System.out.println();
        });
    }

        @Test
    public void regex() throws Exception {
        int[] first = RegExp.findFirst(",$","Shepilov,");
        System.out.println(first[0]+ " " + first[1]);
    }

    @Test
    public void get() throws Exception {
        RegExpsStructure regExpsStructure = initRegExpsStructure();
        File file = new File("names.txt");
        List<BasicRecord> basicRecords = BasicRecordsService.getAllRecords(file, regExpsStructure);
        for (BasicRecord basicRecord : basicRecords) {
            System.out.println("--------------------------------");
            for (Map.Entry<String, String[]> entry : basicRecord.getFields().entrySet()) {
                System.out.println(entry.getKey() + entry.getValue()[1]);
            }
            System.out.println("--------------------------------");
        }
    }

    @Test
    public void getOut() throws IOException {
        RegExpsStructure regExpsStructure = initRegExpsStructure();
        File file = new File("names.txt");
        List<BasicRecord> basicRecords = BasicRecordsService.getAllRecords(file, regExpsStructure);


        List<OutFieldStructure> fieldStructures = new LinkedList<>();
        fieldStructures.add(new OutFieldStructure("Field_1:", OutFieldStructure.VARIABLE_NAME_BORDER + "Date of Birth:", 1));
        fieldStructures.add(new OutFieldStructure("Field_2:", OutFieldStructure.VARIABLE_NAME_BORDER + "Education: tyt moi text", 1));
        fieldStructures.add(new OutFieldStructure("Field_3:", OutFieldStructure.VARIABLE_NAME_BORDER + "Education: Data:" + OutFieldStructure.VARIABLE_NAME_BORDER + "Date of Birth:", 1));

        List<Out> outs = OutService.getOut(basicRecords, fieldStructures);
        for (Out out : outs) {
            System.out.println(out);
        }

    }

    private RegExpsStructure initRegExpsStructure() {
        RegExpsStructure regExpsStructure = new RegExpsStructure("Nationality:", "Nationality:", "");
        RegExpsStructure regExpsStructure1 = new RegExpsStructure("Date of Birth:", "Date of Birth:", "");
        RegExpsStructure regExpsStructure2 = new RegExpsStructure("Place of Birth:", "Place of Birth:", "");
        RegExpsStructure regExpsStructure3 = new RegExpsStructure("Parentage:", "Parentage:", "");
        RegExpsStructure regExpsStructure4 = new RegExpsStructure("Family:", "Family:", "");
        RegExpsStructure regExpsStructure5 = new RegExpsStructure("Education:", "Education:", "");
        RegExpsStructure regExpsStructure6 = new RegExpsStructure("Qualifications:", "Qualifications:", "");
        RegExpsStructure regExpsStructure7 = new RegExpsStructure("Career:", "Career:", "");
        RegExpsStructure regExpsStructure8 = new RegExpsStructure("Publications:", "Publications:", "");
        RegExpsStructure regExpsStructure9 = new RegExpsStructure("Address:", "Address:", "");
        RegExpsStructure regExpsStructure10 = new RegExpsStructure("Telephone:", "Telephone:", "");
        regExpsStructure.addChild(regExpsStructure1);
        regExpsStructure.addChild(regExpsStructure2);
        regExpsStructure.addChild(regExpsStructure3);
        regExpsStructure.addChild(regExpsStructure4);
        regExpsStructure.addChild(regExpsStructure5);
        regExpsStructure.addChild(regExpsStructure6);
        regExpsStructure.addChild(regExpsStructure7);
        regExpsStructure.addChild(regExpsStructure8);
        regExpsStructure.addChild(regExpsStructure9);
        regExpsStructure.addChild(regExpsStructure10);
        return regExpsStructure;
    }

    @Test
    public void getBloc() throws IOException {
        String outFieldValue = "/#Date of Birth:";
        outFieldValue = outFieldValue.replaceAll("/#Date of Birth:", " b. 18 Jan. 1927");
        System.out.println(outFieldValue);
    }

}