package service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import static service.BasicRecordsService.ENCODING;

public class ReplaceService implements Serializable {
    private String abbreviationDelimiter = ";";
    private Map<String, String> abbreviation;

    public ReplaceService() {
        abbreviation = new HashMap<>();
    }

    public void replaceAll(String inputFile, String outFile) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));
        PrintWriter pw = new PrintWriter(new FileOutputStream(outFile));
        String current;
        while ((current = br.readLine()) != null) {
            for (Map.Entry<String, String> entry : abbreviation.entrySet()) {
                current = current.replaceAll("\\b" + entry.getKey().replaceAll("\\.", "\\\\.") + "(?=[^a-zA-Z])", entry.getValue());
            }
            pw.println(current);
        }
        pw.flush();
        br.close();
        pw.close();
    }

    public void loadAbbreviations(String filePath) throws IOException {
        abbreviation.clear();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), ENCODING));
        String str;
        while ((str = reader.readLine()) != null) {
            String[] map = str.split(abbreviationDelimiter);
            abbreviation.put(map[0], map[1]);
        }
        reader.close();
    }

    public String getAbbreviationDelimiter() {
        return abbreviationDelimiter;
    }

    public void setAbbreviationDelimiter(String abbreviationDelimiter) {
        this.abbreviationDelimiter = abbreviationDelimiter;
    }

    public Map<String, String> getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(Map<String, String> abbreviation) {
        this.abbreviation = abbreviation;
    }
}
