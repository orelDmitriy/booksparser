package model;

import java.util.HashMap;
import java.util.Map;

public class BasicRecord {
    private Map<String, String[]> fields;

    public BasicRecord() {
        fields = new HashMap<>();
    }

    public void addField(String name, String[] value) {
        fields.put(name, value);
    }

    public Map<String, String[]> getFields() {
        return fields;
    }
}
