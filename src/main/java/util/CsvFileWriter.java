package util;

import model.Out;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class CsvFileWriter {

    private static final String NEW_LINE_SEPARATOR = "\n";

    public static void writeCsvFile(String fileName, List<String> file_header, List<Out> outs) throws IOException {
        CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);
        FileWriter fileWriter = new FileWriter(fileName);
        CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);
        csvFilePrinter.printRecord(file_header);

        for (Out out : outs) {
            for (String fieldName : file_header) {
                csvFilePrinter.print(out.getFields().get(fieldName));
            }
            csvFilePrinter.println();
        }
        fileWriter.flush();
        fileWriter.close();
        csvFilePrinter.close();
    }
}