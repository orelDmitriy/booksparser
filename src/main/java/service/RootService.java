package service;

import model.BasicRecord;
import model.Out;
import model.OutFieldStructure;
import model.RegExpsStructure;
import util.CsvFileWriter;
import util.SerializationUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

public class RootService implements Serializable {

    private RegExpsStructure rootRegExpsStructure;
    private List<OutFieldStructure> outFieldStructures;
    private ReplaceService replaceService;

    public RootService() {
        rootRegExpsStructure = new RegExpsStructure("root", "root", "");
        outFieldStructures = new LinkedList<>();
        replaceService = new ReplaceService();
    }

    public RegExpsStructure getRootRegExpsStructure() {
        return rootRegExpsStructure;
    }

    public List<OutFieldStructure> getOutFieldStructures() {
        return outFieldStructures;
    }

    public void rewrite(RegExpsStructure regExpsStructure, String name, String patternBegin, String patternEnd) {
        if (Objects.equals(regExpsStructure.getName(), regExpsStructure.getName())) {
            regExpsStructure.setName(name);
            regExpsStructure.setRegExpStart(patternBegin);
            regExpsStructure.setRegExpEnd(patternEnd);
            return;
        }
        for (RegExpsStructure child : rootRegExpsStructure.getChildren()) {
            rewrite(child, name, patternBegin, patternEnd);
        }
    }

    public void deleteRegExpsStructure(RegExpsStructure delete) {
        deleteChildForRegExpsStructure(rootRegExpsStructure, delete);
    }

    private void deleteChildForRegExpsStructure(RegExpsStructure root, RegExpsStructure delete) {
        for (RegExpsStructure child : root.getChildren()) {
            if (child.equals(delete)) {
                root.getChildren().remove(child);
                return;
            }
            deleteChildForRegExpsStructure(child, delete);
        }
    }

    public void addChild(RegExpsStructure regExpsStructure, RegExpsStructure newRegExpsStructure) {
        regExpsStructure.addChild(newRegExpsStructure);
    }

    public void addOutFieldStructure(String fieldName, String pattern, Integer number) {
        outFieldStructures.add(new OutFieldStructure(fieldName, pattern, number));
    }

    public void updateOutFieldStructure(OutFieldStructure current, String name, String pattern, Integer fieldNumber) {
        current.setFieldName(name);
        current.setPattern(pattern);
        current.setNumber(fieldNumber);
    }

    public void deleteOutFieldStructure(OutFieldStructure current) {
        outFieldStructures.remove(current);
    }

    public void process(String inputFile, String outFile) throws IOException {
        List<BasicRecord> basicRecords = BasicRecordsService.getAllRecords(new File(inputFile), rootRegExpsStructure);
        outFieldStructures.sort(Comparator.comparing(OutFieldStructure::getNumber));
        List<String> headers = setHeader();
        List<Out> outs = OutService.getOut(basicRecords, outFieldStructures);
        CsvFileWriter.writeCsvFile(outFile, headers, outs);
    }

    public void replaceAbbreviations(String inputFile, String outFile) throws IOException {
        replaceService.replaceAll(inputFile, outFile);
    }

    private List<String> setHeader() {
        List<String> headers = new LinkedList<>();
        for (OutFieldStructure fieldStructure : outFieldStructures) {
            headers.add(fieldStructure.getFieldName());
        }
        return headers;
    }

    public void saveSettings(String path) throws FileNotFoundException {
        SerializationUtil.serialization(this, path);
    }

    public void loadSettings(String path) throws IOException {

        RootService rootService = SerializationUtil.deserialization(path);
        if (rootService != null) {
            rootRegExpsStructure = rootService.getRootRegExpsStructure();
            outFieldStructures = rootService.getOutFieldStructures();
            replaceService = rootService.getReplaceService();
        }


    }

    public ReplaceService getReplaceService() {
        return replaceService;
    }

    public void loadAbbreviations(String filePath) throws IOException {
        replaceService.loadAbbreviations(filePath);
    }
}
