package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RegExpsStructure implements Serializable {
    private String name;
    private String regExpStart;
    private String regExpEnd;
    private List<RegExpsStructure> children;
    private RegExpsStructure parent;

    public RegExpsStructure(String name, String regExpStarst, String regExpEnd) {
        this.name = name;
        this.regExpStart = regExpStarst;
        this.regExpEnd = regExpEnd;
        children = new LinkedList<>();
    }

    public List<String> getAllRegExpsForEnd() {
        List<String> regExpsForEnd = new LinkedList<>();
        if (!"".equals(regExpEnd)) {
            regExpsForEnd.add(regExpEnd);
        }
        RegExpsStructure current = getParent();
        if (current != null) {
            for (RegExpsStructure child : current.getChildren()) {
                if (!child.equals(this)) {
                    regExpsForEnd.add(child.getRegExpStart());
                }
            }
        }
        return regExpsForEnd;
    }

    private RegExpsStructure getParent() {
        return parent;
    }

    public void addChild(RegExpsStructure child) {
        child.setParent(this);
        children.add(child);
    }

    public String getRegExpEnd() {
        return regExpEnd;
    }

    public String getName() {
        return name;
    }

    public String getRegExpStart() {
        return regExpStart;
    }

    public List<RegExpsStructure> getChildren() {
        return children;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegExpEnd(String regExpEnd) {
        this.regExpEnd = regExpEnd;
    }

    public void setRegExpStart(String regExpStart) {
        this.regExpStart = regExpStart;
    }

    public void setParent(RegExpsStructure parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RegExpsStructure that = (RegExpsStructure) o;

        return name.equals(that.name) && regExpStart.equals(that.regExpStart);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + regExpStart.hashCode();
        return result;
    }
}
