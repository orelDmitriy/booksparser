package service;

import model.BasicRecord;
import model.Out;
import model.OutFieldStructure;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class OutService {

    private static final char BEGIN_VARIABLE = '{';
    private static final char END_VARIABLE = '}';

    private static final char BEGIN_ELEMENT = '[';
    private static final char END_ELEMENT = ']';

    public static List<Out> getOut(List<BasicRecord> basicRecords, List<OutFieldStructure> fieldStructures) {
        List<Out> result = new LinkedList<>();
        for (BasicRecord record : basicRecords) {
            Out outRecord = new Out();
            for (OutFieldStructure fieldStructure : fieldStructures) {
                String outFieldValue = fieldStructure.getPattern();
                outFieldValue = replaceKeys(record, outFieldValue, OutFieldStructure.VARIABLE_NAME_BORDER, 0);
                outFieldValue = replaceKeys(record, outFieldValue, OutFieldStructure.VARIABLE_VALUE_BORDER, 1);
                outFieldValue = searchCommands(outFieldValue);
                outFieldValue = outFieldValue.replaceAll("\\n", " ");
                outRecord.addField(fieldStructure.getFieldName(), outFieldValue.trim());
            }
            result.add(outRecord);
        }
        return result;
    }

    private static String replaceKeys(BasicRecord record, String outFieldValue, String prefix, int positionInArray) {
        int start, end;
        int prefLength = prefix.length();
        while ((start = outFieldValue.indexOf(prefix)) != -1) {
            end = outFieldValue.indexOf(prefix, start + prefLength);
            if (end == -1 || (start + prefLength >= end)) {
                break;
            }
            String key = outFieldValue.substring(start + prefLength, end);
            String value = "";
            for (Map.Entry<String, String[]> field : record.getFields().entrySet()) {
                if (key.equals(field.getKey())) {
                    value = field.getValue()[positionInArray];
                    break;
                }
            }
            outFieldValue = outFieldValue.substring(0, start) + value + outFieldValue.substring(end + prefLength);
        }
        return outFieldValue;
    }

    private static String searchCommands(String str) {
        int start;
        while ((start = str.indexOf(BEGIN_VARIABLE)) != -1) {
            String key = getOneElement(str.substring(start), BEGIN_VARIABLE, END_VARIABLE);
            key = BEGIN_VARIABLE + key + END_VARIABLE;
            try {
                str = str.replace(key, execute(key));
            } catch (StringIndexOutOfBoundsException e){
                break;
            }
        }
        return str;
    }

    private static String execute(String key) {
        String[] rootElements = getRootElements(getOneElement(key, BEGIN_VARIABLE, END_VARIABLE));
        for (int i = 0; i < 4; i++) {
            if (rootElements[i].length() != 0 && rootElements[i].charAt(0) == BEGIN_VARIABLE) {
                rootElements[i] = execute(rootElements[i]);
            }
        }
        if (rootElements[0].equals(rootElements[1])) {
            return rootElements[2];
        } else {
            return rootElements[3];
        }
    }

    private static String[] getRootElements(String key) {
        String[] result = new String[4];
        for (int i = 0; i < 4; i++) {
            result[i] = getOneElement(key, BEGIN_ELEMENT, END_ELEMENT);
            key = key.substring(result[i].length() + 2);
        }
        return result;
    }

    private static String getOneElement(String outFieldValue, Character startChar, Character endChar) {
        int start = outFieldValue.indexOf(startChar);
        int amount = 1;
        int end = -1;
        for (int i = start + 1; i < outFieldValue.length(); i++) {
            if (outFieldValue.charAt(i) == endChar) {
                amount--;
            }
            if (outFieldValue.charAt(i) == startChar) {
                amount++;
            }
            if (amount == 0) {
                end = i;
                break;
            }
        }
        if (start == 0 && end > -1) {
            if (start == end - 1) {
                return "";
            }
            return outFieldValue.substring(start + 1, end);
        } else {
            return outFieldValue;
        }
    }
}
