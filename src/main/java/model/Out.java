package model;

import java.util.HashMap;
import java.util.Map;

public class Out {
    private Map<String, String> fields;

    public Out() {
        fields = new HashMap<>();
    }

    public void addField(String name, String value) {
        fields.put(name, value);
    }

    public Map<String, String> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return fields.toString();
    }
}
