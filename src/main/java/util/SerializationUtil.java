package util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.RegExpsStructure;
import service.RootService;

import java.io.*;

public class SerializationUtil {

    public static void serialization(RootService service, String path) throws FileNotFoundException {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        setNullForParrent(service.getRootRegExpsStructure());
        String json = gson.toJson(service);
        setParrent(service.getRootRegExpsStructure(), null);
        PrintWriter writer = new PrintWriter(new FileOutputStream(path));
        writer.write(json);
        writer.flush();
        writer.close();
    }

    public static RootService deserialization(String path) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String json = "";
        String current;
        while ((current = reader.readLine()) != null) {
            json += current;
        }
        reader.close();
        RootService service = new Gson().fromJson(json, RootService.class);
        setParrent(service.getRootRegExpsStructure(), null);
        return service;
    }

    private static void setParrent(RegExpsStructure structure, RegExpsStructure parrent) {
        structure.setParent(parrent);
        for (RegExpsStructure child : structure.getChildren()) {
            setParrent(child, structure);
        }
    }

    private static void setNullForParrent(RegExpsStructure structure) {
        structure.setParent(null);
        for (RegExpsStructure child : structure.getChildren()) {
            setNullForParrent(child);
        }
    }
}
